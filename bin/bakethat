#!/bin/bash
# vim: set filetype=sh : #

# signal trapping
trap 'trap 2 ; kill -2 $$' 1 2 3 13 15

### setup things so we know where we are ###
HERE=/mnt/bitbake

# setup the configuration & pull in from configuration file if specified.
# source the conf/local.conf if it exists.
if [ -f $HERE/conf/local.conf ]; then
    . $HERE/conf/local.conf
fi
# source the conf file if provided
if [ -f $HERE/conf/$BS_CONFIG ]; then
    . $HERE/conf/$BS_CONFIG
fi

# if not set, figure out how many threads.
NCPU=`grep -c processor /proc/cpuinfo`
BB_THREADS=${BB_THREADS:-$NCPU}

### Manifest Configuration  ###
MANIFEST_PROTO=${MANIFEST_PROTO:-ssh}
MANIFEST_HOST=${MANIFEST_HOST:-bitbucket.org}
MANIFEST_REPO=${MANIFEST_REPO:-devonit/manifest.git}
MANIFEST_FILE=${MANIFEST_FILE:-default.xml}
MANIFEST_BRANCH=${MANIFEST_BRANCH:-master}
MANIFEST_GITURI=
function generate_manifest_clone_uri()
{
    MANIFEST_PROTO=${MANIFEST_PROTO,,}
    if [ "$MANIFEST_PROTO" == "ssh" ]; then
        MANIFEST_GITURI="ssh://git@$MANIFEST_HOST/"
    elif [ "$MANIFEST_PROTO" == "https" ]; then
        MANIFEST_GITURI="https://$MANIFEST_HOST/"
    else
        echo "MANIFEST_PROTO: Unknown Protocol for Git: '$MANIFEST_PROTO'"
        exit 1
    fi;
}

# go to the mounted build directory
cd /mnt/bitbake/build 
echo "Currently in `pwd`"
if [ -z "$SKIP_SYNC" ]; then
    if [ -z "$SKIP_INIT" ]; then
        # init the repo we're configured with.
        generate_manifest_clone_uri
        echo "-- repo init --"
        repo init -u ${MANIFEST_GITURI}${MANIFEST_REPO} -b "$MANIFEST_BRANCH" \
            -m $MANIFEST_FILE 2>&1 | tee -a repo-${BS_TUPLE}.log
        rv=${PIPESTATUS[0]}
        if [[ $rv -ne 0 ]]; then
            echo "Failed to init the repository."
            exit $rv
        fi
    fi
    # sync the repo
    echo "-- repo sync --"
    repo sync 2>&1 | tee -a repo-${BS_TUPLE}.log
    rv=${PIPESTATUS[0]}
    if [[ $rv -ne 0 ]]; then
        echo "Failed to sync the repository."
        exit $rv
    fi
fi

# set OE_ROOT if not set.
OE_ROOT=${OE_ROOT:-$PWD}
# cd to OE_ROOT since that is where conf/local.conf is.
cd $OE_ROOT
# change the conf/local.conf
echo "CONF_VERSION = \"1\""          > conf/local.conf
echo "DISTRO = \"$BB_DISTRO\""      >> conf/local.conf
echo "DISTRO_TYPE ?= \"$BB_DISTRO_TYPE\""  >> conf/local.conf 
echo "DL_DIR = \"/mnt/bitbake/dl\""          >> conf/local.conf
echo "SSTATE_DIR = \"/mnt/bitbake/sstate\""  >> conf/local.conf
echo "DEPLOY_IMAGE_DIR = \"/mnt/bitbake/build/image\""   >> conf/local.conf
echo "MACHINE = \"$BB_MACHINE\"" >> conf/local.conf
echo "BB_NUMBER_THREADS = \"$BB_THREADS\"" >> conf/local.conf
echo "PARALLEL_MAKE = \"-j$BB_THREADS\"" >> conf/local.conf

# source the conf file if provided
if [ -f ${HERE}/conf/${BS_CONFIG}.append ]; then
    echo "Appending ${HERE}/conf/${BS_CONFIG}.append to conf/local.conf"
    cat ${HERE}/conf/${BS_CONFIG}.append >> conf/local.conf
fi

# TODO: change the conf/bblayers.conf (needed?)

# override at command line for interactive mode
if [ $# -gt 0 ]; then
    BB_PACKAGE=$@
fi
# disable |tee if NOLOG has a value.
TEE=1
if [ "${NOLOG}x" != "x" ]; then 
    TEE=0
fi
DATESTAMP=`date +'%Y%m%d%H%M%S'`
echo "-- bitbake $BB_PACKAGE --"
if [ $TEE -gt 0 ]; then
    bitbake $BB_PACKAGE 2>&1 | tee --append ${BS_TUPLE}-${DATESTAMP}.log
else
    bitbake $BB_PACKAGE
fi

exit ${PIPESTATUS[0]}
