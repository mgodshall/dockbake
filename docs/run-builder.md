# run-builder
Script to run bitbake in the bitbake container, using a unified layout inside the container, with host mapped (and controlled) mounts for storage.

Supports running with `sudo` so for mapping of calling user's `$HOME`

## Usage

```
$ bin/run-builder -h
run-builder usage:
 -h : this help
 -x : echo the resulting behaivor, don't execute.
 -c : configuration file to source for environment.
 -i : run interactive shell for this configuration.
 -p : package/command for bitbake. Overrides BB_PACKAGE.
 -t : tuple extension, e.g. 'mine' results in 'x-x-x-mine'
 -e : set an environment variable in the container. e.g. THIS=THAT
 -k : pass the -k (--continue) argument to bitbake
```

- `-x` Echo resulting docker command for verification/examination, do not execute.
- `-c` Configuration file to be sources when run, optional but suggested for use with `sudo`. e.g. `conf/7.2.2-unified` Can be passed multiple times if you decide to stack configurations.
- `-i` Run an interactive shell for this configuration.
- `-p` Override the `$BB_PACKAGE` with this value. Can be used as `-p "-c cleanall"` or `-p "virtual/kernel -c clean"` as examples.
- `-t` Set a postfix for the naming tuple, in the event you wish to try multiple builds of similarly named configurations.
- `-e` Set an environment variable inside the container.
- `-k` Set the --continue argument for bitbake call.

## Function
Set sane defaults for values not present in the environment, configure tuple naming, and map paths for docker-based container to use.